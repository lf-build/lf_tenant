﻿using System;

namespace LendFoundry.Tenant
{
    public class TenantNotFoundException : Exception
    {
        public TenantNotFoundException(string message):base(message)
        {

        }

        public TenantNotFoundException():base("Tenant not found")
        {

        }
    }
}
