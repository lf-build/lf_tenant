﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Abstractions;

namespace LendFoundry.Tenant
{
    public class TenantService : ITenantService
    {
        public TenantService(ITenantRepository repository, IConfigurationServiceFactory configurationServiceFactory, ITokenHandler tokenHandler, IEventHubClientFactory eventHubFactory)
        {
            Repository = repository;
            ConfigurationServiceFactory = configurationServiceFactory;
            TokenHandler = tokenHandler;
            EventHubFactory = eventHubFactory;
        }

        #region Variables

        private ITenantRepository Repository { get; }
        private IConfigurationServiceFactory ConfigurationServiceFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private IEventHubClientFactory EventHubFactory { get; }

        #endregion

        public async Task<IEnumerable<ITenant>> GetAll()
        {
            return await Repository.GetAll();
        }

        public async Task<ITenant> Get(string id)
        {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            var entry = await Repository.Get(id);

            if (entry == null)
                throw new TenantNotFoundException($"{id} tenant not found");

            return entry;
        }

        public async Task<ITenantDetails> Register(ITenant tenant, TenantServiceConfiguration tenantServiceConfiguration)
        {
            if (tenant == null)
                throw new ArgumentNullException(nameof(tenant));

            if (tenant.Name == null)
                throw new ArgumentNullException(nameof(tenant.Name));

            if (tenant.Email == null)
                throw new ArgumentNullException(nameof(tenant.Email));

            if (tenant.Phone == null)
                throw new ArgumentNullException(nameof(tenant.Phone));

            if (tenant.Website == null)
                throw new ArgumentNullException(nameof(tenant.Website));

            EnsureTenantConfigurationIsValid(tenantServiceConfiguration);

            PublishTenantConfiguration(tenant, tenantServiceConfiguration);

            var result = await Repository.Create(tenant);

            var eventHub = EventHubFactory.Create(GetTokenReader(result.Name));
            await eventHub.Publish("TenantAdded", new TenantAdded(result.Name, result));
            var tenantDetails = new TenantDetails(result)
            {
                Token = GetToken(result.Name)
            };
            return tenantDetails;
        }

        public async Task<ITenant> Update(ITenant tenant, TenantServiceConfiguration tenantServiceConfiguration)
        {
            if (tenant == null)
                throw new ArgumentNullException(nameof(tenant));

            if (tenant.Id == null)
                throw new ArgumentNullException(nameof(tenant.Id));

            if (tenant.Name == null)
                throw new ArgumentNullException(nameof(tenant.Name));

            if (tenant.Email == null)
                throw new ArgumentNullException(nameof(tenant.Email));

            if (tenant.Phone == null)
                throw new ArgumentNullException(nameof(tenant.Phone));

            if (tenant.Website == null)
                throw new ArgumentNullException(nameof(tenant.Website));

            var entry = await Get(tenant.Id);
            if (entry == null)
                throw new TenantNotFoundException($"{tenant.Id} tenant not found");

            entry.Name = tenant.Name;
            entry.Email = tenant.Email;
            entry.Phone = tenant.Phone;
            entry.Website = tenant.Website;

            EnsureTenantConfigurationIsValid(tenantServiceConfiguration);

            PublishTenantConfiguration(entry, tenantServiceConfiguration);

            await Repository.Update(entry);

            var eventHub = EventHubFactory.Create(GetTokenReader(tenant.Name));
            await eventHub.Publish("TenantUpdated", new TenantUpdated(tenant.Name, entry));
            var tenantDetails = new TenantDetails(entry)
            {
                Token = GetToken(entry.Name)
            };
            return entry;
        }

        public async Task<ITenant> Activate(string id)
        {
            var tenant = await Get(id);
            if (tenant.IsActive)
                throw new ArgumentException($"Tenant: {id} is already active");
            tenant.IsActive = true;
            await Repository.Update(tenant);

            var eventHub = EventHubFactory.Create(GetTokenReader(tenant.Name));
            await eventHub.Publish("TenantActivated", new TenantActivated(id, tenant));

            return tenant;
        }

        public async Task<ITenant> Deactivate(string id)
        {
            var tenant = await Get(id);
            if (!tenant.IsActive)
                throw new ArgumentException($"Tenant: {id} is already deactive");
            tenant.IsActive = false;
            await Repository.Update(tenant);

            var eventHub = EventHubFactory.Create(GetTokenReader(tenant.Name));
            await eventHub.Publish("TenantDeactivated", new TenantDeactivated(id, tenant));

            return tenant;
        }

        private void PublishTenantConfiguration(ITenant tenant, TenantServiceConfiguration tenantServiceConfiguration)
        {
            var configurationService = ConfigurationServiceFactory.Create<TenantServiceConfiguration>(Settings.ServiceName, GetTokenReader(tenant.Name));
            configurationService.Set(tenantServiceConfiguration);
        }

        private ITokenReader GetTokenReader(string tenant)
        {
            var reader = new StaticTokenReader(GetToken(tenant));
            return reader;
        }

        private string GetToken(string tenant)
        {
            var serviceName = Settings.ServiceName;
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            return token.Value;
        }

        private void EnsureTenantConfigurationIsValid(TenantServiceConfiguration tenantServiceConfiguration)
        {
            if (tenantServiceConfiguration == null)
                throw new ArgumentNullException(nameof(tenantServiceConfiguration));

            if (string.IsNullOrWhiteSpace(tenantServiceConfiguration.ConnectionString))
                throw new ArgumentNullException(nameof(tenantServiceConfiguration.ConnectionString));

            if (string.IsNullOrWhiteSpace(tenantServiceConfiguration.Timezone))
                throw new ArgumentNullException(nameof(tenantServiceConfiguration.Timezone));

            if (!TenantTime.Timezones.ContainsKey(tenantServiceConfiguration.Timezone))
                throw new ArgumentException($"Invalid value of  timezone: {(tenantServiceConfiguration.Timezone)}");
        }
    }
}