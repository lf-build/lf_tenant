﻿using System;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif

using LendFoundry.Tenant.Api.ViewModels;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using System.Threading.Tasks;
using LendFoundry.Tenant.Abstractions;
using LendFoundry.EventHub;

namespace LendFoundry.Tenant.Api.Controllers
{
    /// <summary>
    /// Tenant service controller
    /// </summary>
    [Route("/")]
    public class TenantController : Controller
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TenantController"/> class.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="logger">The logger.</param>
        public TenantController(ITenantService service, ILogger logger)
        {
            Logger = logger;
            Service = service;
        }

        private ILogger Logger { get; }

        private ITenantService Service { get; }


        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(ITenant[]), 200)]

        public async Task<IActionResult> GetAll()
        {
            return Ok(await Service.GetAll());
        }

        /// <summary>
        /// Gets the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ITenant), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> Get(string id)
        {
            return await ExecuteAsync(async () => Ok(await Service.Get(id)));
        }

        /// <summary>
        /// Registers the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(typeof(ITenant), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Register([FromBody]TenantViewModel model)
        {
            return await ExecuteAsync(async () =>
            {
                var tenant = ToTenant(model);
                if (model == null)
                    throw new ArgumentNullException(nameof(model));
                var result = await Service.Register(tenant, model.TenantServiceConfiguration);
                return Ok(result);
            });
        }

        /// <summary>
        /// Updates the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Update([FromRoute]string id, [FromBody]TenantViewModel model)
        {
            return await ExecuteAsync(async () =>
            {
                var tenant = ToTenant(model, id);
                if (model == null)
                    throw new ArgumentNullException(nameof(model));
                var result = await Service.Update(tenant, model.TenantServiceConfiguration);
                return GetStatusCodeResult(204);
            });
        }

        /// <summary>
        /// Activates the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpPost("{id}/activate/")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Activate([FromRoute]string id)
        {
            return await ExecuteAsync(async () =>
            {
                var result = await Service.Activate(id);
                return GetStatusCodeResult(204);
            });

        }

        /// <summary>
        /// Deactivates the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpPost("{id}/deactivate/")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Deactivate(string id)
        {
            return await ExecuteAsync(async () =>
            {
                var result = await Service.Deactivate(id);
                return GetStatusCodeResult(204);
            });
        }

        private IActionResult GetStatusCodeResult(int statusCode)
        {
#if DOTNET2
            return new StatusCodeResult(statusCode);
#else
            return new HttpStatusCodeResult(statusCode);
#endif
        }

        private Tenant ToTenant(TenantViewModel tenantViewModel, string id = null)
        {
            if (tenantViewModel == null)
                return null;
            return new Tenant()
            {
                Email = tenantViewModel.Email,
                Name = tenantViewModel.Name,
                IsActive = tenantViewModel.IsActive,
                Phone = tenantViewModel.Phone,
                Website = tenantViewModel.Website,
                Id = id
            };
        }

        private async Task<IActionResult> ExecuteAsync(Func<Task<IActionResult>> expression)
        {
            try
            {
                return await expression();
            }
            catch (ArgumentException ex)
            {
                return new ErrorResult(400, ex.Message);
            }
            catch (TenantNotFoundException ex)
            {
                return new ErrorResult(404, ex.Message);
            }
        }
    }
}