﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.EventHub.Client;
using LendFoundry.Tenant.Abstractions;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Mongo;
using LendFoundry.Configuration.Client;
using System.Collections.Generic;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif

namespace LendFoundry.Tenant.Api
{
    internal class Startup
    {

        public void ConfigureServices(IServiceCollection services)
        {
            // Register the Swagger generator, defining one or more Swagger documents
#if DOTNET2

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "Tenant"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme() 
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> 
                { 
					{ "Bearer", new string[]{} }
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.Tenant.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
#else
            services.AddSwaggerDocumentation();
#endif

            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddEventHub(Settings.ServiceName);
            services.AddConfigurationService<TenantServiceConfiguration>(Settings.ServiceName);
            services.AddTransient<ITenantRepository, TenantRepository>();
            services.AddScoped<ITenantService, TenantService>();
            services.AddSingleton<IMongoConfiguration, MongoConfiguration>();
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		app.UseCors(env);

            // Enable middleware to serve generated Swagger as a JSON endpoint.
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "Tenant Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif

            
            app.UseErrorHandling();

            // PLEASE DO NOT USE THE CODE BELOW TILL WE FINISH THE DC/OS DEPLOY
            //if (!env.IsDevelopment())
            //    app.UseSecurityControl();

            app.UseRequestLogging();
            app.UseMvc();
            app.UseConfigurationCacheDependency();
        }
    }
}