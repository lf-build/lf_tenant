﻿namespace LendFoundry.Tenant.Api
{

    internal interface IMappingEngine
    {
        void CreateMappings();
    }
}
