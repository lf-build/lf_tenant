using LendFoundry.Tenant.Abstractions;
using LendFoundry.Tenant.Mongo;

namespace LendFoundry.Tenant.Api
{
    internal class MongoConfiguration : IMongoConfiguration
    {
        public MongoConfiguration()
        {
            ConnectionString = Settings.MongoConnectionString;
            Database = Settings.MongoDatabase;
        }

        public string ConnectionString { get; set; }
        public string Database { get; set; }
    }
}