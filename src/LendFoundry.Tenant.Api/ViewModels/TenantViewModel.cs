﻿using LendFoundry.Tenant.Abstractions;

namespace LendFoundry.Tenant.Api.ViewModels
{
    /// <summary>
    /// Tenant
    /// </summary>
    public class TenantViewModel
    {
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Phone
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Website
        /// </summary>
        public string Website { get; set; }


        /// <summary>
        /// IsActive
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// TenantServiceConfiguration 
        /// </summary>
        public TenantServiceConfiguration TenantServiceConfiguration { get; set; }

    }
}
