﻿namespace LendFoundry.Tenant.Mongo
{
    public interface IMongoConfiguration
    {
        string ConnectionString { get; set; }
        string Database { get; set; }
    }
}