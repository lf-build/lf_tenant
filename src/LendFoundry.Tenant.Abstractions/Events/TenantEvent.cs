﻿using Newtonsoft.Json;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Tenant.Abstractions
{
    public class TenantEvent
    {
        public string Id { get; set; }
        public string Name { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ITenant, Tenant>))]
        public ITenant Tenant { get; set; }

        public TenantEvent()
        {
        }

        public TenantEvent(string id)
        {
            Id = id;
        }

        public TenantEvent(string name, ITenant tenant)
        {
            Name = name;
            Tenant = tenant;
        }
    }
}
