﻿namespace LendFoundry.Tenant.Abstractions
{
    public class TenantActivated : TenantEvent
    {
        public TenantActivated(string id, ITenant tenant) : base(id, tenant)
        {
        }
    }
}
