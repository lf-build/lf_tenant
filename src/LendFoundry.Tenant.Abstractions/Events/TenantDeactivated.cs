﻿namespace LendFoundry.Tenant.Abstractions
{
    public class TenantDeactivated : TenantEvent
    {
        public TenantDeactivated(string id, ITenant tenant) : base(id, tenant)
        {
        }
    }
}
