﻿namespace LendFoundry.Tenant.Abstractions
{
    public class TenantUpdated : TenantEvent
    {
        public TenantUpdated(string name, ITenant tenant) : base(name, tenant)
        {
        }
    }
}
