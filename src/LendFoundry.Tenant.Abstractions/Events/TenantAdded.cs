﻿namespace LendFoundry.Tenant.Abstractions
{
    public class TenantAdded : TenantEvent
    {
        public TenantAdded(string name, ITenant tenant) : base(name, tenant)
        {
        }
    }
}
