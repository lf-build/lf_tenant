﻿namespace LendFoundry.Tenant.Abstractions
{
    public class TenantDetails : Tenant, ITenantDetails
    {
        public TenantDetails()
        {

        }
        public TenantDetails(ITenant tenant)
        {
            Id = tenant.Id;
            Name = tenant.Name;
            Email = tenant.Email;
            Phone = tenant.Phone;
            Website = tenant.Website;
            IsActive = tenant.IsActive;
        }

        public string Token { get; set; }
    }
}
