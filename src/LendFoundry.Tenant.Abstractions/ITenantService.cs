﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Tenant.Abstractions
{
    public interface ITenantService
    {
        Task<IEnumerable<ITenant>> GetAll();

        Task<ITenant> Get(string id);

        Task<ITenantDetails> Register(ITenant tenant, TenantServiceConfiguration tenantServiceConfiguration);

        Task<ITenant> Update(ITenant tenant, TenantServiceConfiguration tenantServiceConfiguration);

        Task<ITenant> Activate(string id);

        Task<ITenant> Deactivate(string id);
    }
}
