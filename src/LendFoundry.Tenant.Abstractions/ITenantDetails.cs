﻿namespace LendFoundry.Tenant.Abstractions
{
    public interface ITenantDetails : ITenant
    {
        string Token { get; set; }
    }
}