using System;

namespace LendFoundry.Tenant.Abstractions
{
    public static class Settings
    {
        private const string Prefix = "TENANT";

        public static string ServiceName { get; } = Prefix.ToLower();

        public static string MongoConnectionString { get; } = Environment.GetEnvironmentVariable($"{Prefix}_MONGO_CONNECTION") ?? "mongodb://mongo:27017";

        public static string MongoDatabase { get; } = Environment.GetEnvironmentVariable($"{Prefix}_MONGO_DATABASE") ?? "tenant";
    }
}