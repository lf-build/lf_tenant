﻿using System.Collections.Generic;

namespace LendFoundry.Tenant.Abstractions
{
    public class TenantServiceConfiguration
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string Timezone { get; set; }
        public string Phone { get; set; }
        public string PaymentTimeLimit { get; set; }
        public string Website { get; set; }
        public string ConnectionString { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
    }
}
