namespace LendFoundry.Tenant.Abstractions
{
    public interface ITenant
    {
        string Id { get; set; }

        string Name { get; set; }

        string Email { get; set; }

        string Phone { get; set; }

        string Website { get; set; }

        bool IsActive { get; set; }
    }
}
