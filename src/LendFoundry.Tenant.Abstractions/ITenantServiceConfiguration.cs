﻿using System.Collections.Generic;

namespace LendFoundry.Tenant.Abstractions
{
    public interface ITenantServiceConfiguration
    {
        string IsActive { get; set; }
        string Email { get; set; }
        string Name { get; set; }
        string Timezone { get; set; }
        string Phone { get; set; }
        string PaymentTimeLimit { get; set; }
        string Website { get; set; }
        string ConnectionString { get; set; }
        Dictionary<string, string> Dependencies { get; set; }
    }
}

