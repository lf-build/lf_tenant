﻿using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Abstractions;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace LendFoundry.Tenant.Tests
{
    public class TenantServiceTests
    {
        private FakeTenantRepository fakeTenantRepository = new FakeTenantRepository(new List<ITenant>() {
            {
                new Tenant(){
                Email = "default@gmail.com",
                Id = "default",
                IsActive = true,
                Name = "default",
                Phone = "default",
                Website = "http://www.default.com"
                } },
                new Tenant(){
                Email = "inactive@gmail.com",
                Id = "inactive",
                IsActive = true,
                Name = "inactive",
                Phone = "inactive",
                Website = "http://www.inactive.com"
                            },
                new Tenant(){
                Email = "inactive@gmail.com",
                Id = "test",
                IsActive = false,
                Name = "test",
                Phone = "inactive",
                Website = "http://www.inactive.com"
                            }
        });

        private ITenantService GetTenantService()
        {
            var tokenHandlerMock = new Mock<ITokenHandler>();
            tokenHandlerMock.Setup(s => s.Issue(It.IsAny<string>(), It.IsAny<string>())).Returns(new Token());
            var configurationService = new Mock<IConfigurationService<TenantServiceConfiguration>>();
            var configurationServiceFactory = new Mock<IConfigurationServiceFactory>();
            configurationService.Setup(s => s.Set(It.IsAny<TenantServiceConfiguration>())).Returns(true);
            var x = configurationServiceFactory.Setup(s => s.Create<TenantServiceConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>())).Returns(configurationService.Object);

            var eventHub = new Mock<IEventHubClient>();
            eventHub.Setup(s => s.Publish(It.IsAny<TenantAdded>())).ReturnsAsync(true);
            var eventHubClientFactoryMock = new Mock<IEventHubClientFactory>();
            eventHubClientFactoryMock.Setup(e => e.Create(It.IsAny<ITokenReader>())).Returns(eventHub.Object);
            return new TenantService(fakeTenantRepository, configurationServiceFactory.Object, tokenHandlerMock.Object, eventHubClientFactoryMock.Object);
        }

        [Fact]
        public void CreateTenantShouldSuccess()
        {
            var tenantService = GetTenantService();

            var response = tenantService.Register(new Tenant()
            {
                Email = "test@gmail.com",
                Id = "tenant-01",
                IsActive = true,
                Name = "tenant-01",
                Phone = "9999",
                Website = "http://www.google.com"
            }, new TenantServiceConfiguration() { ConnectionString = "mongodb://mongo:27017", Timezone = "Asia/Calcutta" }).Result;

            Assert.Equal("tenant-01", response.Id);
        }

        [Fact]
        public void RegisterThrowsExceptionOnNullArgument()
        {
            var tenantService = GetTenantService();
            var exception = Assert.Throws<AggregateException>(() =>
            {
                var response = tenantService.Register(null, null).Result;
            });
            Assert.IsType<ArgumentNullException>(exception.InnerExceptions[0]);
        }

        [Fact]
        public void RegisterThrowsExceptionOnMissingRequiredFields()
        {
            var tenantService = GetTenantService();
            var exception = Assert.Throws<AggregateException>(() =>
            {
                var response = tenantService.Register(new Tenant()
                {
                    Email = "nayan.gp@sigmainfo.net",
                }, null).Result;
            });
            Assert.IsType<ArgumentNullException>(exception.InnerExceptions[0]);
        }

        [Fact]
        public void GetTenantShouldReturnTenant()
        {
            var tenantService = GetTenantService();
            var response = tenantService.Get("default").Result;
            Assert.Equal("default", response.Id);
        }

        [Fact]
        public void GetTenantThrowArgumentExceptionOnArgumentNull()
        {
            var tenantService = GetTenantService();
            var exception = Assert.Throws<AggregateException>(() =>
            {
                var response = tenantService.Get(null).Result;
            });
            Assert.IsType<ArgumentNullException>(exception.InnerExceptions[0]);
        }

        [Fact]
        public void GetTenantThrowNotFoundWhenItDoesNotExist()
        {
            var tenantService = GetTenantService();
            var exception = Assert.Throws<AggregateException>(() =>
            {
                var response = tenantService.Get("NotExistTenant").Result;
            });
            Assert.IsType<TenantNotFoundException>(exception.InnerExceptions[0]);
        }

        [Fact]
        public void GetAllTenantShouldReturnAllTenant()
        {
            var tenantService = GetTenantService();
            var response = tenantService.GetAll().Result.ToList();
            Assert.Equal(3, response.Count);
        }

        [Fact]
        public void UpdateTenantShouldUpdateTenant()
        {
            var tenantService = GetTenantService();
            var response = tenantService.Register(new Tenant()
            {
                Email = "test@gmail.com",
                Id = "tenant-01",
                IsActive = true,
                Name = "tenant-01",
                Phone = "9999",
                Website = "http://www.google.com"
            }, new TenantServiceConfiguration() { ConnectionString = "mongodb://mongo:27017", Timezone = "Asia/Calcutta" }).Result;

            Assert.Equal("tenant-01", response.Id);

            var expectedTenant = new Tenant() { Id = "tenant-01", Email = "testnew@gmail.com", Name = "tenant-01-new", Phone = "99999", Website = "http://www.tenant.com" };
            var responseOfUpdate = tenantService.Update(expectedTenant, new TenantServiceConfiguration() { ConnectionString = "mongodb://mongo:27017", Timezone = "Asia/Calcutta" }).Result;
            Assert.True(responseOfUpdate != null);
            var updatedTenant = tenantService.Get("tenant-01").Result;
            Assert.Equal(expectedTenant.Name, updatedTenant.Name);
            Assert.Equal(expectedTenant.Email, updatedTenant.Email);
            Assert.Equal(expectedTenant.Phone, updatedTenant.Phone);
            Assert.Equal(expectedTenant.Website, updatedTenant.Website);
        }

        [Fact]
        public void UpdateThrowsExceptionOnNullArgument()
        {
            var tenantService = GetTenantService();
            var exception = Assert.Throws<AggregateException>(() =>
            {
                var response = tenantService.Update(null,null).Result;
            });
            Assert.IsType<ArgumentNullException>(exception.InnerExceptions[0]);
        }

        [Fact]
        public void UpdateThrowsExceptionOnMissingRequiredFields()
        {
            var tenantService = GetTenantService();
            var exception = Assert.Throws<AggregateException>(() =>
            {
                var response = tenantService.Update(new Tenant()
                {
                    Email = "nayan.gp@sigmainfo.net",
                },null).Result;
            });
            Assert.IsType<ArgumentNullException>(exception.InnerExceptions[0]);
        }

        [Fact]
        public void UpdateTenantThrowNotFoundWhenItDoesNotExist()
        {
            var tenantService = GetTenantService();
            var exception = Assert.Throws<AggregateException>(() =>
            {
                var response = tenantService.Update(new Tenant()
                {
                    Email = "default@gmail.com",
                    IsActive = true,
                    Name = "default",
                    Phone = "default",
                    Website = "http://www.default.com",
                    Id = "NotExistTenant"
                },null).Result;
            });
            Assert.IsType<TenantNotFoundException>(exception.InnerExceptions[0]);
        }

        [Fact]
        public void ActivateTenantThrowExceptionWhenTenantNotFound()
        {
            var tenantService = GetTenantService();
            var exception = Assert.Throws<AggregateException>(() =>
            {
                var response = tenantService.Activate("notFound").Result;
            });
            Assert.IsType<TenantNotFoundException>(exception.InnerExceptions[0]);
        }

        [Fact]
        public void DeActivateTenantThrowExceptionWhenTenantNotFound()
        {
            var tenantService = GetTenantService();
            var exception = Assert.Throws<AggregateException>(() =>
            {
                var response = tenantService.Deactivate("notFound").Result;
            });
            Assert.IsType<TenantNotFoundException>(exception.InnerExceptions[0]);
        }

        [Fact]
        public void ActivateTenantShouldSuccess()
        {
            var tenantService = GetTenantService();
            var response = tenantService.Activate("test").Result;
            Assert.True(response != null);
        }

        [Fact]
        public void DeActivateTenantShouldSuccess()
       {
            var tenantService = GetTenantService();
            var response = tenantService.Deactivate("inactive").Result;
            Assert.True(response != null);
        }

    }
}
